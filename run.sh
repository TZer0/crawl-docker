#!/bin/bash
source settings.rc

if [ ! -d "$DGLDIR_VOLUME" ]; then
	mkdir -p "$DGLDIR_VOLUME"
fi
if [ ! -d "$CRAWL_MASTER_VOLUME" ]; then
	mkdir -p "$CRAWL_MASTER_VOLUME"
fi
if [ ! -f "$VERSIONSDB_VOLUME" ]; then
	touch "$VERSIONSDB_VOLUME"
fi
if [ ! -d "$GAMES_VOLUME" ]; then
	mkdir -p "$GAMES_VOLUME"
fi

docker run -it -v "$GAMES_VOLUME:/usr/games/" -v "$DGLDIR_VOLUME:/dgldir" -v "$CRAWL_MASTER_VOLUME:/crawl-master" -v "$VERSIONSDB_VOLUME:/crawl-versions.db3" -p 8080:8080 -p 2222:22 "$DOCKER_TAG" /bin/bash
