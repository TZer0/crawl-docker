chmod u+s /bin/dgamelaunch
mkdir -p $FOLDERS
chown -R "crawl:crawl" $FOLDERS

/home/crawl-dev/dgamelaunch-config/bin/dgl create-crawl-gamedir
/utils/savedirs.sh $CRAWLVERSIONS

/utils/dgldirs.sh $CRAWLVERSIONS
/home/crawl-dev/dgamelaunch-config/bin/dgl publish --confirm

#/home/crawl-dev/dgamelaunch-config/bin/dgl create-versions-db
SIZE=$(du -sb /crawl-versions.db3 | awk '{ print $1 }')
if ((SIZE==0)); then
cat /home/crawl-dev/dgamelaunch-config/crawl-build/version-db-schema.sql | sqlite3 /crawl-versions.db3
fi
/home/crawl-dev/dgamelaunch-config/bin/dgl update-trunk
cp /home/crawl-dev/dgamelaunch-config/crawl-build/crawl-git-repository/crawl-ref/source/webserver/templates/*.html /crawl-master/webserver/templates/

/utils/createuserdb.sh
